import { Link } from 'react-router-dom';
import axios from 'axios';
import { useNavigate } from "react-router-dom";

export function Header() {
  const navigation = useNavigate();
  const token = localStorage.getItem('token');
  
  const logout = () =>{
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    axios.defaults.headers.common['Authorization'] = '';
    navigation('/signIn');
  }

  return (
    <header className="header">
      <nav className="header_navigate">
          {
            token ? (
              <button type='button' onClick={logout} >Выйти</button>
            ) : (
                <Link to='/signIn'></Link>
            )
          }
      </nav>
    </header> 
  )
}

export default Header;
