import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  add,
  fetchTodos,
  selectTodos,
} from './todoListSlice';
import styles from './TodoList.module.css';
import { Task } from './Task';
import { Navigate } from 'react-router-dom';

type User = {
  id: string;
  username: string;
}

export function TodoList() {
  const dispatch:any = useDispatch();
  const todos = useSelector(selectTodos);
  const [title, setTitle] = useState('Enter todo!');
  const jwtToken = localStorage.getItem('token');
  let userId = localStorage.getItem('userId') || '';

  useEffect(() => {
    dispatch(fetchTodos())
  }, []);

  return (
    <div>
      {
        jwtToken ? (
          <div>
            <ul>
              {todos.map(t => <Task key={t.id} todo={t}/>)}
            </ul>
            <div className={styles.row}>
              <input type="text"
                onChange={e => setTitle(e.target.value)}
                value={title}></input>
              <button type='button'
                onClick={async () => {
                    await add({ title, completed: false }, userId); 
                    window.location.reload();
                  }
                }
                >➕</button>
            </div>
          </div>
        ) : (
          <Navigate to={"/signIn"} />
        )
      }
    </div>
  );
}
