import React from 'react';
import logo from './logotype.svg';
import './App.css';
import { TodoList } from './features/todo/TodoList';
import Login from './auth/Login';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Registration from './auth/Registration';
import Header from './header/Header';

function App() {  
  return (
    <BrowserRouter>
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <Header/>
          <Routes>
            <Route path='/' element={<TodoList />}/>
            <Route path='signIn' element={<Login />}/>
            <Route path='registration' element={<Registration />}/>
          </Routes>
        </header>
      </div>
    </BrowserRouter>
  );
}

export default App;
