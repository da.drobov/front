
import React, { FormEvent, useState } from "react";
import { useNavigate } from "react-router";
import axios from 'axios';
import { Link } from "react-router-dom";


const Registration = () => {
  const navigate = useNavigate();
  const [form, setForm] = useState({ username: '', password: '' });

  async function fetchForm(e: FormEvent) {
    e.preventDefault();

    const response = await axios.post('https://coursework123457890.herokuapp.com/auth/signUp', form);
    
    navigate('/signIn');
  }
  return (
    <form className="auth_form" onSubmit={fetchForm}>
      <div className="auth_input-block">
        <label htmlFor="login">Логин</label>
        <input
          value={form.username}
          onChange={e => setForm({ ...form, username: e.target.value })}
          id="login"
          type="text"
          placeholder="Логин"
          required
        />
      </div>
      <div className="auth_input-block">
        <label htmlFor="pass">Пароль</label>
        <input
          value={form.password}
          onChange={e => setForm({ ...form, password: e.target.value })}
          id="pass"
          type="password"
          placeholder="Пароль"
          required
        />
      </div>
      <div className="auth_result_input-block">
        <button>Зарегистрироваться</button>
        <Link to='/signIn'>Войти</Link>
      </div>
    </form>
  );
};

export default Registration;