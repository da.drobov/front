import axios from "axios";
import React, { FormEvent, useState } from "react";
import { useNavigate } from "react-router";
import { Link } from "react-router-dom";


const Login = () => {
  const navigate = useNavigate();
  const [form, setForm] = useState({ username: '', password: '' });

  async function fetchForm(e: FormEvent) {
    e.preventDefault();

    try {
      const { data } = await axios.post('https://coursework123457890.herokuapp.com/auth/signIn', form);

      localStorage.setItem('token', data.jwtToken);
      localStorage.setItem('userId', data.user.id);

      axios.defaults.headers.common['Authorization'] = `Bearer ${data.jwtToken}`;
      
      navigate('/');
    } catch (e) {
      alert(e);
    }
  }

  return (
    <form className="auth_form" onSubmit={fetchForm}>
      <div className="auth_input-block">
        <label htmlFor="login">Логин</label>
        <input
          value={form.username}
          onChange={e => setForm({ ...form, username: e.target.value })}
          id="login"
          type="text"
          placeholder="Логин"
          required
        />
      </div>
      <div className="auth_input-block">
        <label htmlFor="pass">Пароль</label>
        <input
          value={form.password}
          onChange={e => setForm({ ...form, password: e.target.value })}
          id="pass"
          type="password"
          placeholder="Пароль"
          required
        />
      </div>
      <div className="auth_result_input-block">
        <button>Войти</button>
        <Link to='/registration'>Зарегистрироваться</Link>
      </div>
    </form>
  );
};

export default Login;